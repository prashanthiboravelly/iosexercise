//
//  ViewController.h
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>


@end

