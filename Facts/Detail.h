//
//  Detail.h
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Detail : NSObject

@property (retain, nonatomic) NSString *title;
@property (retain, nonatomic) NSString *desc;
@property (retain, nonatomic) NSString *imageRef;

- (id)initWithdetailDictionary:(NSDictionary *)detail;
@end
