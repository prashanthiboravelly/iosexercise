//
//  DetailTableViewCell.h
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Detail;

@interface DetailTableViewCell : UITableViewCell

- (void)setValuesForLabelsFromDetail:(Detail *)detail;
- (void)setImageForImageView:(UIImage *)image;


@end
