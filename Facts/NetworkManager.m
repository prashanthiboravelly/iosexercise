//
//  NetworkManager.m
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"

@implementation NetworkManager

#pragma mark - Creating singleton object

+ (NetworkManager *)sharedNetworkManager {
    static dispatch_once_t p = 0;
    __strong static id _sharedObject = nil;
    dispatch_once(&p, ^{
        _sharedObject = [[self alloc] init];
    });
    return _sharedObject;
}

#pragma mark - Fetching the data from server

- (void)fetchDetailsFromServerWithURL:(NSURL *)url
                          withSuccess:(void (^)(NSDictionary * responseObject))success
                       andWithFailure:(void (^)(NSError *error))failure {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
        [request setValue:@"text/plain" forHTTPHeaderField:@"Content-Type"];
        
        NSURLResponse *response = nil;
        NSError *error = nil;
        
        //Get the response
        NSData * responseObj = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        [request release];
        if (!error) {
            //Get the string out of data
            NSString *encodedString = [[NSString alloc] initWithData:responseObj encoding:NSASCIIStringEncoding];
            
            //Create string back with UTF8 encoding
            NSData *stringData = [encodedString dataUsingEncoding:NSUTF8StringEncoding];
            [encodedString release];
            
            //Converting to dictionary
            NSDictionary * responseDictionary = [NSJSONSerialization JSONObjectWithData:stringData options:0 error:&error];
            
            if (!error) {
                success(responseDictionary);
            } else {
                failure(error);
            }
            
        } else {
            failure(error);
        }
    });
}


- (void)fetchImageWithURL:(NSURL *)url
              withSuccess:(void (^)(UIImage * image))success
           andWithFailure:(void (^)(NSError *error))failure {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        //Get the image
        UIImage *image = [[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:url]] autorelease];
        NSError *error = nil;

        if(image){
            success(image);
        }else{
            failure(error);
        }
    });
}


@end
