//
//  ViewController.m
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import "ViewController.h"
#import "NetworkManager.h"
#import "Detail.h"
#import "DetailTableViewCell.h"
#import "NSString+DynamicHeight.h"

#define serverURL @"https://www.dropbox.com/s/g41ldl6t0afw9dv/facts.json?dl=1"
#define labelWidth 235.0f
#define imageHeight 76.0f
#define spacing 3.0f
#define titleLabelFont 20.0f
#define descLabelFont 15.0f

@interface ViewController () {
    BOOL isPullToRefreshActive;
}

@property (retain, nonatomic) NSMutableArray *detailsArray;
@property (retain, nonatomic) UITableView *tableView;
@property (retain, nonatomic) UIRefreshControl *refreshControl;
@property (retain, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (nonatomic, strong) NSCache *imageCache;


- (void)fetchDetails;

@end


@implementation ViewController

#pragma mark
#pragma mark - UIViewController lifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //Adding table view
    self.tableView = [[[UITableView alloc] initWithFrame:self.view.bounds] autorelease];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.hidden = YES;
    [self.view addSubview:self.tableView];
    
    // Add activity Indicator
    self.activityIndicator = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
    self.activityIndicator.center = CGPointMake(self.view.frame.size.width / 2.0, self.view.frame.size.height / 2.0);
    self.activityIndicator.hidesWhenStopped = YES;
    [self.view addSubview:self.activityIndicator];
    
    //Add Refresh Control
    self.refreshControl = [[[UIRefreshControl alloc] init] autorelease];
    [self.refreshControl addTarget:self action:@selector(pulltoRefreshInitiated) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
    
    self.detailsArray = [[[NSMutableArray alloc] init] autorelease];
    
    self.imageCache = [[NSCache alloc] init];
    
    
    [self fetchDetails];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.activityIndicator startAnimating];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc {
    [_tableView release];
    [_refreshControl release];
    [_detailsArray release];
    [_activityIndicator release];
    [super dealloc];
}


#pragma mark
#pragma mark - Fetching the json file

- (void)fetchDetails {
    NSURL *url = [NSURL URLWithString:serverURL];
    __block typeof(self) _blockSelf = self;
    
    [[NetworkManager sharedNetworkManager] fetchDetailsFromServerWithURL:url withSuccess:^(NSDictionary *responseObject) {
        
        // Get main queue before doing any UI stuff
        [_blockSelf stopLoading];
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if(self.tableView.isHidden)
                self.tableView.hidden = NO;
            
            //converting to model objects
            NSArray *results = [responseObject valueForKey:@"rows"];
            
            for (NSDictionary *dictionary in results) {
                Detail *detail = [[Detail alloc] initWithdetailDictionary:dictionary];
                
                //Adding object to detailsArray
                [self.detailsArray addObject:detail];
                [detail release];
            }
            
            _blockSelf.title = [responseObject objectForKey:@"title"];
            
            //Reload the table
            [self.tableView reloadData];
        });
        
    } andWithFailure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self displayAlertWithMessage:[error localizedDescription]];
        });
        [_blockSelf stopLoading];
    }];
    
    
}

#pragma mark
#pragma mark - UIRefreshControl handlers

- (void)pulltoRefreshInitiated {
    isPullToRefreshActive = YES;
    [self.refreshControl beginRefreshing];
    [self fetchDetails];
}

- (void)stopLoading {
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([_activityIndicator isAnimating]) {
            [_activityIndicator stopAnimating];
        }
        
        if (isPullToRefreshActive) {
            [self.refreshControl endRefreshing];
            isPullToRefreshActive = NO;
        }
    });
}


#pragma mark
#pragma mark - tableView DataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.detailsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    DetailTableViewCell *cell = (DetailTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        cell = [[[DetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier] autorelease];
    }
    
    Detail *detail = [self.detailsArray objectAtIndex:indexPath.row];
    
    //setting title and description
    [cell setValuesForLabelsFromDetail:detail];
    
    //caching the image
    UIImage *cachedImage =   [self.imageCache objectForKey:indexPath];;
    if (cachedImage) {
        [cell setImageForImageView:cachedImage];
    }
    else {
        [cell setImageForImageView:[UIImage imageNamed:@"NoImage.jpeg"]];
        
        //fetching image and setting it to the cell
        if(detail.imageRef) {
            NSURL *url = [NSURL URLWithString:detail.imageRef];
            [[NetworkManager sharedNetworkManager] fetchImageWithURL:url withSuccess:^(UIImage *image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (image) {
                        [cell setImageForImageView:image];
                        [self.imageCache setObject:image forKey:indexPath];
                    }
                });
                
            } andWithFailure:^(NSError *error) {
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    [self displayAlertWithMessage:@"One of the image is not retrieved properly"];
//                });
            }];
        }
    }
    return cell;
}


#pragma mark
#pragma mark - tableView Delegate Method

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    //Calculating the height of cell
    Detail *detail = [self.detailsArray objectAtIndex:indexPath.row];
    
    CGFloat heightOfTitle = [NSString calculateHeightOfText:detail.title WithFont:titleLabelFont ForWidth:labelWidth];
    CGFloat heightOfDescription = [NSString calculateHeightOfText:detail.desc WithFont:descLabelFont ForWidth:labelWidth];
    
    CGFloat LabelsHeight = (3*spacing)+ heightOfTitle+heightOfDescription;
    
    return MAX(LabelsHeight, heightOfTitle+imageHeight+(2*spacing));
}


#pragma mark
#pragma mark - Display Alert view

- (void)displayAlertWithMessage:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Error" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [alertView release];
}

@end

