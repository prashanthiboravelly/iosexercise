//
//  DetailTableViewCell.m
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import "DetailTableViewCell.h"
#import "Detail.h"
#import "NSString+DynamicHeight.h"

#define labelWidth 235.0f
#define imageHeight 76.0f
#define spacing 3.0f
#define titleLabelFont 20.0f
#define descLabelFont 15.0f

@interface DetailTableViewCell  ()

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *descriptionLabel;
@property (nonatomic, retain) UIImageView *profileImageView;


@end

@implementation DetailTableViewCell

#pragma mark - lifecycle methods

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        //Creating title Label
        self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        [self.titleLabel setFont:[UIFont systemFontOfSize:20.0f]];
        self.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.titleLabel setTextAlignment:NSTextAlignmentLeft];
        [self.titleLabel setTextColor:[UIColor blueColor]];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.numberOfLines = 0;
        self.titleLabel.text = @"No Title";
        [self.contentView addSubview:self.titleLabel];
        
        //Creating description Label
        self.descriptionLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        [self.descriptionLabel setFont:[UIFont systemFontOfSize:15.0f]];
        [self.descriptionLabel setTextAlignment:NSTextAlignmentLeft];
        self.descriptionLabel.backgroundColor = [UIColor clearColor];
        self.descriptionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.descriptionLabel setTextColor:[UIColor blackColor]];
        self.descriptionLabel.text = @"No description";
        self.descriptionLabel.numberOfLines = 0;
        [self.contentView addSubview:self.descriptionLabel];
        
        //Creating ImageView
        self.profileImageView =[[[UIImageView alloc] initWithFrame:CGRectZero] autorelease];
        self.profileImageView.backgroundColor = [UIColor clearColor];
        self.profileImageView.frame = CGRectMake(241, spacing, imageHeight, imageHeight);
        self.profileImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.profileImageView];
    }
    
    return self;
}


- (void)dealloc {
    [_titleLabel release];
    [_descriptionLabel release];
    [_profileImageView release];
    [super dealloc];
}


#pragma mark - setters for labels and Image

- (void)setValuesForLabelsFromDetail:(Detail *)detail {
    
    //setting the values and frames
    NSString *titleString = detail.title;
    NSString *descriptionString = detail.desc;
    
    if(titleString) {
        self.titleLabel.text = titleString;
    }
    CGFloat heightOfTitle = [NSString calculateHeightOfText:self.titleLabel.text WithFont:self.titleLabel.font.pointSize ForWidth:labelWidth];
    self.titleLabel.frame = CGRectMake(2*spacing,spacing, labelWidth, heightOfTitle);
    
    if(descriptionString) {
        self.descriptionLabel.text = descriptionString;
    }
    
    CGFloat heightOfDescription = [NSString calculateHeightOfText:self.descriptionLabel.text WithFont:self.descriptionLabel.font.pointSize ForWidth:labelWidth];

    self.descriptionLabel.frame = CGRectMake(2*spacing,self.titleLabel.frame.size.height+(2*spacing), labelWidth, heightOfDescription);
    
    
}


- (void)setImageForImageView:(UIImage *)image {
    self.profileImageView.image = image;
}


@end
