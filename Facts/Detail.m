//
//  Detail.m
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import "Detail.h"

@implementation Detail


- (id)initWithdetailDictionary:(NSDictionary *)detail {
    self = [super init];
    if (self) {
        
        NSString *description = [detail valueForKey:@"description"];
        if (description && ![description isEqual:[NSNull null]] && description.length >0) {
            self.desc = description;
        } else {
            self.desc = nil;
        }
        
        NSString *title = [detail valueForKey:@"title"];
        if (title && ![title isEqual:[NSNull null]] && title.length >0) {
            self.title = title;
        } else {
            self.title = nil;
        }
        
        NSString *imageRef = [detail valueForKey:@"imageHref"];
        if (imageRef && ![imageRef isEqual:[NSNull null]] && imageRef.length >0) {
            self.imageRef = imageRef;
        } else {
            self.imageRef = nil;
        }
        
    }
    return self;
}



- (void)dealloc {
    [_title release];
    [_desc release];
    [_imageRef release];
    [super dealloc];
}


@end
