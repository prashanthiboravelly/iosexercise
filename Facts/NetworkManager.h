//
//  NetworkManager.h
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkManager : NSObject

+ (NetworkManager *)sharedNetworkManager;

- (void)fetchDetailsFromServerWithURL:(NSURL *)url
                          withSuccess:(void (^)(NSDictionary * responseObject))success
                       andWithFailure:(void (^)(NSError *error))failure;

- (void)fetchImageWithURL:(NSURL *)url
                          withSuccess:(void (^)(UIImage * image))success
                       andWithFailure:(void (^)(NSError *error))failure;

@end
