//
//  NSString+DynamicHeight.m
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSString+DynamicHeight.h"

@implementation NSString (DynamicHeight)

#pragma mark - Calculates the string height for the given width and font

+ (double)calculateHeightOfText:(NSString *)text WithFont:(double )fontSize ForWidth:(double )width{
    CGFloat height = 0.0f;
    
    if(text) {
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        
        // Create the attributes dictionary with the font and paragraph style
        NSDictionary *attributes = @{
                                     NSFontAttributeName:[UIFont systemFontOfSize:fontSize],
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        
        // Call boundingRectWithSize:options:attributes:context for the string
        CGRect textRect = [text boundingRectWithSize:CGSizeMake(width, 999999.0f)
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attributes
                                             context:nil];
        [paragraphStyle release];
        
        height = textRect.size.height;
    }
    
    return height;
  
}

@end