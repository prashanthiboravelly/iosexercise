//
//   NSString+DynamicHeight
//  Facts
//
//  Created by Prashanthi on 2/12/15.
//  Copyright (c) 2015 Prashanthi. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (DynamicHeight)

+ (double)calculateHeightOfText:(NSString *)text WithFont:(double )fontSize ForWidth:(double )width;

@end
